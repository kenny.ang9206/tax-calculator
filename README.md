# Online Pajak Coding Challenge - Tax Calculator
<img src="https://user-images.githubusercontent.com/50238797/68066384-f41a3200-fd71-11e9-9b19-1853d743c406.png" width="600px" />
<br/>
Online Pajak Coding Challenge

Built by - [Kenny Ang](https://github.com/kach92)(GitHub Link)

## Objective
To create a program that calculates the Personal Income Tax in Indonesia
based on the taxpayer’s annual income and tax reliefs.

## Technologies used
- HTML, CSS, JavaScript ES6
- Node.Js
- HTTP Server
- Jest

## Installation Instructions
1. Fork/clone the project then install all the dependencies of the project using
```
npm install
````
2. To run jest for testing, run the following command
```
npm run test
````
3. To run app, first run the server with
```
npm start
````
4. Then go to the following link or click [Here](http://localhost:8080/)
```
http://localhost:8080/
````

## Functions of App
- User can input their salary.
- User can select whether the salary they input is monthly or annually. The app will calculate them by itself.
- User can select their marital status. Different marital status gives different tax relief.
- User can see their final tax to be paid based on their annual income and status.
- User can check taxation scheme.

## Approach Taken
- Started by creating 3 main functions - to convert monthly to annually income, tax calculation by income, and tax relief calculation. All three functions are done in seperate js files and exported
- Install Jest and do testing for each of these functions with edge cases
- Setup index.html and style.css for a layout for user to input their income and select income type as well as marital status choices.
- link script.js to html, in order to link module type script into index.html, need to utilize http-server to bypass CORS.
- Setup some babel plugins for test env due to Jest unable to transpile ES6 import/export.
- Import all main functions into script.js and start piecing them together with the necessary input/output in index.html using DOM.
- Refactor by placing all main functions into a fullCalculator function instead, and also add test cases to it.
- Do debugging and refactorizing.
- Added a modal to show taxation scheme

### Disclaimer
This app serves as a coding challenge given by Online Pajak only.