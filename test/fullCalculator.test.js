const fullCalculator = require('../src/fullCalculator.js').fullCalculator;


test('returns -1 if salary is not a number OR less than 0', () => {
    expect(fullCalculator('',"monthly","TK0")).toBe(-1);
    expect(fullCalculator('abc',"monthly","TK0")).toBe(-1);
    expect(fullCalculator([],"monthly","TK0")).toBe(-1);
    expect(fullCalculator([1,2,3],"monthly","TK0")).toBe(-1);
    expect(fullCalculator(-100000,"monthly","TK0")).toBe(-1);
    expect(fullCalculator(-100000,"monthly","TK0")).toBe(-1);
    expect(fullCalculator(NaN,"monthly","TK0")).toBe(-1);
});

test('returns -1 if incorrect income type', () => {
    expect(fullCalculator(25000000,"april","TK0")).toBe(-1);
});

test('returns -1 if incorrect code', () => {
    expect(fullCalculator(25000000,"monthly","AA")).toBe(-1);
});

test('return correct tax post-calculation', () => {
    expect(fullCalculator(0,"monthly","TK0")).toBe(0);
    expect(fullCalculator(25000000,"monthly","TK0")).toBe(31900000);
    expect(fullCalculator(100000000,"monthly","K2")).toBe(284750000);
    expect(fullCalculator(700000000,"annually","K3")).toBe(133400000);
    expect(fullCalculator(510000000,"annually","K0")).toBe(82875000);
    expect(fullCalculator(435000000,"annually","K1")).toBe(63000000);
});


