const taxRelieve = require('../src/taxRelieve.js').taxRelieve;


test('returns -1 if input is not a number OR less than 0', () => {
    expect(taxRelieve('','TK0')).toBe(-1);
    expect(taxRelieve('abc','TK0')).toBe(-1);
    expect(taxRelieve([],'TK0')).toBe(-1);
    expect(taxRelieve([1,2,3],'TK0')).toBe(-1);
    expect(taxRelieve(-100000,'TK0')).toBe(-1);
    expect(taxRelieve(NaN,'TK0')).toBe(-1);
});


test('returns -1 code is not in standard list', () => {
    expect(taxRelieve(70000000,'TK1')).toBe(-1);
    expect(taxRelieve(70000000,123)).toBe(-1);
    expect(taxRelieve(70000000,[])).toBe(-1);
    expect(taxRelieve(70000000,{})).toBe(-1);
});

test('returns correct value for different code', () => {
    expect(taxRelieve(100000000,'TK0')).toBe(46000000);
    expect(taxRelieve(2150000000,'K3')).toBe(2078000000);
    expect(taxRelieve(500000000,'K0')).toBe(441500000);
    expect(taxRelieve(7887776662,'K1')).toBe(7824776662);
    expect(taxRelieve(800000000,'K2')).toBe(732500000);
});

