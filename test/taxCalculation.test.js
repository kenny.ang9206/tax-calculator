const taxCalculation = require('../src/taxCalculation.js').taxCalculation;


test('returns -1 if input is not a number OR less than 0', () => {
    expect(taxCalculation('')).toBe(-1);
    expect(taxCalculation('abc')).toBe(-1);
    expect(taxCalculation([])).toBe(-1);
    expect(taxCalculation([1,2,3])).toBe(-1);
    expect(taxCalculation(-100000)).toBe(-1);
    expect(taxCalculation(NaN)).toBe(-1);
});

test('for annual income between 0 and 50M', () => {
    expect(taxCalculation(0)).toBe(0.00);
    expect(taxCalculation(50000000)).toBe(2500000);
    expect(taxCalculation(32258000)).toBe(1612900);
});

test('for annual income between 50M and 250M', () => {
    expect(taxCalculation(200000000)).toBe(25000000);
});

test('for annual income between 250M and 500M', () => {
    expect(taxCalculation(300000000)).toBe(45000000);
});

test('for annual income more than 500M', () => {
    expect(taxCalculation(700000000)).toBe(155000000);
});

