const monthToAnnualConverter = require('../src/monthToAnnual.js').monthToAnnualConverter;

test('returns -1 if input is not a number OR less than 0', () => {
    expect(monthToAnnualConverter('')).toBe(-1);
    expect(monthToAnnualConverter('abc')).toBe(-1);
    expect(monthToAnnualConverter([])).toBe(-1);
    expect(monthToAnnualConverter([1,2,3])).toBe(-1);
    expect(monthToAnnualConverter(-1)).toBe(-1);
    expect(monthToAnnualConverter(-10000)).toBe(-1);
    expect(monthToAnnualConverter(NaN)).toBe(-1);
});


test('returns correct annual salary', () => {
    expect(monthToAnnualConverter(40000)).toBe(480000);
    expect(monthToAnnualConverter(0)).toBe(0);
    expect(monthToAnnualConverter(70000)).toBe(840000);
});


