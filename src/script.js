import {fullCalculator} from "./fullCalculator.js";

const input = document.getElementById("income-input");
const output = document.getElementById('tax-output');
const calculateButton = document.getElementById('calculate-button');
const incomeType = document.getElementById('income-type');
const maritalStatus = document.getElementById('marital-status');

function calculate(){
    let result = fullCalculator(Number(input.value),incomeType.value,maritalStatus.value);
    result === -1 ? output.value = "Invalid Input" :  output.value = result.toLocaleString(undefined, {maximumFractionDigits:2}) ;
}

calculateButton.addEventListener("click",calculate);
input.addEventListener("change",calculate);
incomeType.addEventListener("change",calculate);
maritalStatus.addEventListener("change",calculate);

