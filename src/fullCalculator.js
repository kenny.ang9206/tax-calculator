import {monthToAnnualConverter} from "./monthToAnnual.js";
import {taxCalculation} from "./taxCalculation.js";
import {taxRelieve} from "./taxRelieve.js";

export function fullCalculator(salary,incomeType,code){
    if(salary<0 || typeof salary !== "number" || isNaN(salary)){return -1}
    let result = incomeType === "annually"? salary : incomeType === "monthly" ? monthToAnnualConverter(salary) : -1;
    result = taxRelieve(result,code);
    result = taxCalculation(result);
    return result;
}

