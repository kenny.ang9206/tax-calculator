
const tax = [
    {
        min : 500000000,
        taxFromMinus : 95000000,
        taxRate : 0.3
    },
    {
        min : 250000000,
        taxFromMinus : 32500000,
        taxRate : 0.25
    },
    {
        min : 50000000,
        taxFromMinus : 2500000,
        taxRate : 0.15
    },
    {
        min : 0,
        taxFromMinus : 0,
        taxRate : 0.05
    }
]

export function taxCalculation(annualIncome){

    if(annualIncome<0 || typeof annualIncome !== "number" || isNaN(annualIncome)){return -1}
    if(annualIncome === 0){return 0}

    for(let i = 0 ; i < tax.length  ; i++){
        if(annualIncome > tax[i].min){
            return (annualIncome - tax[i].min) * tax[i].taxRate + tax[i].taxFromMinus;
        }
    }

}

