export function monthToAnnualConverter(salary){
    if(salary<0 || typeof salary !== "number" || isNaN(salary)){return -1}
    return salary*12;
}

