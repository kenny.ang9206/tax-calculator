const codeTable = {
    "TK0" : 54000000,
    "K0" : 58500000,
    "K1" : 63000000,
    "K2" : 67500000,
    "K3" : 72000000
}

export function taxRelieve(annualIncome,code){
    if(annualIncome<0 || typeof annualIncome !== "number" || isNaN(annualIncome) || !(code in codeTable)){return -1};

    annualIncome -= codeTable[code];
    return annualIncome >0 ? annualIncome : 0 ;
}

